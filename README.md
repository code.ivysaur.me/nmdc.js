# nmdc.js

![](https://img.shields.io/badge/written%20in-Javascript%20%28node.js%29-blue)

A node.js library for chatting on an NMDC server.

Install using `npm install nmdc`.

For documentation and downloads, see the npm project page: https://npmjs.org/package/nmdc 

Tags: nmdc

## Changelog

2017-02-04 1.6.1
- Feature: Separate ClientVersion and Tag in user/connection options
- Fix an issue with supplied user tags missing the `V:` parameter
- [⬇️ nmdc-1.6.1.tgz](dist-archive/nmdc-1.6.1.tgz) *(5.33 KiB)*


2016-03-14 1.6.0
- Feature: Add `tls` option to support TLS support for `nmdcs://` hubs
- [⬇️ nmdc-1.6.0.tgz](dist-archive/nmdc-1.6.0.tgz) *(5.27 KiB)*


2015-08-20 1.5.1
- Fix compilation error in contributed patches for 1.5.0
- [⬇️ nmdc-1.5.1.tgz](dist-archive/nmdc-1.5.1.tgz) *(5.22 KiB)*


2015-08-02 1.5.0
- Feature: Add `onStateChange` callback API (contributed)
- Feature: Add `getHubName` and `getIsConnected` APIs (contributed)
- Feature: Add `shouldInstantConnect` option to control default construction behaviour (contributed)
- [⬇️ nmdc-1.5.0.tgz](dist-archive/nmdc-1.5.0.tgz) *(5.22 KiB)*


2015-02-14 1.4.0
- Feature: Automatically follow hub redirects (with default-false option `follow_redirects`)
- Feature: Add option `ignore_chat_failures` to suppress thrown exceptions when sending a chat message fails
- Fix an issue with Verlihub's handshake causing nmdc.js to erroneously send multiple `$Version` messages
- Fix an issue spamming the error log with unused `$Search` and `$ConnectToMe` protocol messages
- Fix an issue not protocol-unescaping system messages
- Fix an issue not passing non-star system messages
- Fix an issue upon recieving multiple blank protocol messages
- [⬇️ nmdc-1.4.0.tgz](dist-archive/nmdc-1.4.0.tgz) *(4.96 KiB)*


2014-03-08 1.3.0
- Feature: Report connection errors in the `onSystem` handler
- Feature: Add docblocks for publicly-exported functions
- Fix a number of issues with automatic reconnections, and use TCP keepalives instead of empty protocol messages
- [⬇️ nmdc-1.3.0.tgz](dist-archive/nmdc-1.3.0.tgz) *(4.61 KiB)*


2013-11-26 1.2.0a
- Fix version number in default client tag
- Fix missing URL in `package.json` documentation
- [⬇️ nmdc-1.2.0a.tgz](dist-archive/nmdc-1.2.0a.tgz) *(4.12 KiB)*


2013-11-16 1.2.0
- Feature: Add `onUserCommand` callback API
- Feature: Add `Nmdc.raw()` API
- Fix an issue that could cause keepalives to not be sent
- [⬇️ nmdc-1.2.0.tgz](dist-archive/nmdc-1.2.0.tgz) *(4.09 KiB)*


2013-05-05 1.1.0a
- Fix markdown syntax in 1.1.0 release
- [⬇️ nmdc-1.1.0a.tgz](dist-archive/nmdc-1.1.0a.tgz) *(3.81 KiB)*


2013-05-05 1.1.0
- BREAKING: Prevent construction without `new`
- BREAKING: Remove `onGotHubname` callback API
- Feature: Socket keepalives and timeouts
- Fix a crash when the connection was lost
- Fix `onConnect` being `say`-able
- [⬇️ nmdc-1.1.0.tgz](dist-archive/nmdc-1.1.0.tgz) *(3.81 KiB)*


2012-05-05 1.0.1a
- Feature: Add `onGotHubname`, `onSystem` callback APIs
- Fix correctness issues
- [⬇️ nmdc-1.0.1a.tgz](dist-archive/nmdc-1.0.1a.tgz) *(2.78 KiB)*


2012-04-15 1.0.0
- Initial public release
- [⬇️ nmdc-1.0.0.tgz](dist-archive/nmdc-1.0.0.tgz) *(2.71 KiB)*


## See Also

- Port to Golang: [libnmdc.go](https://code.ivysaur.me/libnmdc.go/)
- Port to Python 2.7: https://bitbucket.org/SquareSkeleton/nmdcpy (contributed)
- Port to ActionScript: vendored in [flexdc](https://code.ivysaur.me/flexdc/)
- Port to Java (J2ME): vendored in [jmdc](https://code.ivysaur.me/jmdc/)
- Port to C++: vendored in [cedc](https://code.ivysaur.me/cedc/), [dcwebui](https://code.ivysaur.me/dcwebui/), [ut2dc](https://code.ivysaur.me/ut2dc/), [mudc](https://code.ivysaur.me/mudc/), [pspdc](https://code.ivysaur.me/pspdc/)
